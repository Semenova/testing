package PageFactory;

import org.openqa.selenium.WebDriver;

public class AbstractFactoryPage {
    protected static WebDriver driver;
    protected String baseUrl;

    public AbstractFactoryPage() {
    }
    public AbstractFactoryPage(WebDriver driver) {
        this.driver = driver;
    }


    public WebDriver getDriver() {
        return driver;
    }

    public String getBaseUrl() {
        return baseUrl;
    }
}
