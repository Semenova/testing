package PageFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainFactoryPage extends AbstractFactoryPage {
    protected String baseUrl = "http://www.aliexpress.com/";

    @FindBy(xpath = "//a/span[@class='logo-base']")
    private static WebElement Logo;
    @FindBy(xpath = "//div/input[@id='search-key']")
    private static WebElement qq;

    public MainFactoryPage(WebDriver driver) {
        super(driver);
        driver.get(baseUrl);
        PageFactory.initElements(driver, this);
    }

    public static void ClickLogo() {
        Logo.click();
    }

    public static SearchFactoryPage search(String q) {
        qq.sendKeys(q);
        qq.sendKeys(Keys.ENTER);
        return new SearchFactoryPage(driver);
    }

}
