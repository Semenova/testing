package PageFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ResultFactoryPage extends AbstractFactoryPage {

    @FindBy(xpath = "//a/span[@class='logo-base']")
    private static WebElement Logo;

    public ResultFactoryPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void ClickLogo() { Logo.click();
    }


}
