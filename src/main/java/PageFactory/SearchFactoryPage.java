package PageFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.*;

import java.util.List;

public class SearchFactoryPage extends AbstractFactoryPage {

    @FindBy(xpath = "//a/span[@class='logo-base']")
    private static WebElement Logo;

    @FindBy(xpath = ".//*[@id='hs-list-items']/li[1]/div[1]/div/div[1]/h3/a")
    private static WebElement result;

    public SearchFactoryPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void ClickLogo() {
        Logo.click();
    }

    public ResultFactoryPage selectresult() {
        result.click();
        return new ResultFactoryPage(driver);
    }

}
