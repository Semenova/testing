package PageObjectPattern;

import org.apache.xpath.operations.String;
import org.openqa.selenium.WebDriver;

public class AbstractPage {
    protected static WebDriver driver;
    protected String baseUrl;

    public AbstractPage() {
    }
    public AbstractPage(WebDriver driver) {
        this.driver = driver;
    }


    public WebDriver getDriver() {
        return driver;
    }

    public String getBaseUrl() {
        return baseUrl;
    }
}
