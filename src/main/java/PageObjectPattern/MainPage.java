package PageObjectPattern;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends AbstractPage {
    protected String baseUrl = "http://www.aliexpress.com/";

    public MainPage(WebDriver driver) {
        super(driver);
        driver.get(baseUrl);
    }

    public static void ClickLogo() {
        driver.findElement(By.xpath("//a/span[@class='logo-base']")).click();
    }

    public static SearchPage search(String q) {
        driver.findElement(By.xpath("//div/input[@id='search-key']")).sendKeys(q);
        driver.findElement(By.xpath("//div/input[@id='search-key']")).sendKeys(Keys.ENTER);
        return new SearchPage(driver);
    }

}
