package PageObjectPattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchPage extends AbstractPage {

    public SearchPage(WebDriver driver) {
        super(driver);
    }

    public void ClickLogo() {
        driver.findElement(By.xpath("//a/span[@class='logo-base']")).click();
    }

    public ResultPage selectresult() {
        driver.findElement(By.xpath(".//*[@id='hs-list-items']/li[1]/div[1]/div/div[1]/h3/a")).click();
        return new ResultPage(driver);
    }

}
