import PageFactory.MainFactoryPage;
import PageFactory.ResultFactoryPage;
import PageFactory.SearchFactoryPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class PageFactoryTest {
    WebDriver driver;
    MainFactoryPage mainPage;
    SearchFactoryPage searchPage;
    ResultFactoryPage resultPage;

    @BeforeClass
    public void setup() {
        driver = new FirefoxDriver();
    }

    @Test(groups = "test")
    public void testLoginTrue() {
        mainPage = new MainFactoryPage(driver);
        searchPage = mainPage.search("iphone");
        resultPage = searchPage.selectresult();
        Assert.assertTrue(driver.getCurrentUrl().contains("201556"));
    }

    @AfterClass
    public void tearDown() {
        driver.close();
    }
}
