import PageObjectPattern.ResultPage;
import PageObjectPattern.SearchPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import PageObjectPattern.MainPage;


public class PageObjectPatternTest {
    WebDriver driver;
    MainPage mainPage;
    SearchPage searchPage;
    ResultPage resultPage;

    @BeforeClass
    public void setup() {
        driver = new FirefoxDriver();
    }

    @Test(groups = "test")
    public void gtestLoginTrue() {
        mainPage = new MainPage(driver);
        searchPage = MainPage.search("iphone");
        resultPage = searchPage.selectresult();
        Assert.assertTrue(driver.getCurrentUrl().contains("201556"));
    }

    @AfterClass
    public void tearDown() {
        driver.close();
    }
}
