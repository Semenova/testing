import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class SimpleTest{
    WebDriver driver;

    @BeforeClass
    public void setup() {
        driver = new FirefoxDriver();
    }

    @Test(groups = "test")
    public void SimpleAction() {
        driver.get("http://www.weather.com");
        driver.findElement(By.xpath("//input[@name='search']")).sendKeys("Omsk");
        driver.findElement(By.xpath("//div/p[text()='Omsk, Omsk Oblast, Russia']")).click();
    }

    @AfterClass
    public void tearDown() {
        driver.close();
    }
}
